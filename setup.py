# coding=utf-8

from setuptools import setup, find_packages

__version__ = "unknown"
exec(open('stfs/version.py').read())
print "stmount: " + __version__

setup(name="stmount",
      version = __version__,
      author = 'Jörgen Karlsson',
      author_email = 'jorgen@karlsson.com',
      description = 'Syncthing User Mode File System',
      long_description = open('README.txt').read(),
      packages = find_packages(),
      url = "https://bitbucket.org/jorkar",
      install_requires = [
        "fusepy>=2.0.2",
        "requests>=2.7.0",
        "python-dateutil>=2.4.2",
        ],
      #test_suite = 'tests',
      scripts=['stmount'],
      classifiers = [
        "Development Status :: 3 - Alpha",
        "Topic :: Home Automation",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7",
        "Unreleased :: Do not upload to PYPI"
        ]
      )
