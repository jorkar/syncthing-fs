syncthing-fs
============

Proof of Concept for a user mode fs for syncthing

::

    Copyright (C) 2014 Jörgen Karlsson. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Supported platforms
-------------------

Currently tested on Linux/debian with Python 2.7.

Installation
------------

Prerequsites
~~~~~~~~~~~~

Linux with installed fuse.

Known faults
------------

This is a prototype. Error checking is not complete. Use with care.

-  Only reading is supported. Writing works, however updates are not
   always done correct to the cluster.
-  Deleting is not supported. Odd things happen currently. Solution:
   dismount and mount.
-  The .stignore file will contain more and more rows. This can slow
   down performance of Syncthing. Solution: adjust manually.
-  Large and huge folders will take some time when accessing a file
   first time (eg via ls/dir). This is due to a lot of data beeing
   fetched from Syncthing.

FAQ
---

How do I unmount?
-----------------

use

''' fusermount -u

.. raw:: html

   <dir> 

'''

I get the error "fusermount - failed to open /etc/fuse.conf - Permission denied"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

cause
^^^^^

The error occurs, because only root and members of the group fuse have
the permissions which are required to run fuse. #### solution

groups You will get a list of groups you are a member of and “fuse” will
most likely be missing. If it is a different user who has the problem,
just use his/her username.

Solution The problem can be easily fixed by adding the user to the fuse
group:

''' sudo addgroup fuse (Replace with your username or the the username
of the user you want to add to fuse)

After that, if the user is currently logged in, the user needs to log
out and back in for the changes to take effect. The changes can be
confirmed running

''' groups

“fuse” should now be listed.

Release notes
-------------

0.1.0
~~~~~

Proof of concept
