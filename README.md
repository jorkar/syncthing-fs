# syncthing-fs

Proof of Concept for a user mode fs for syncthing. By mounting a large folder from syncthing, where all or most files are excluded, files will be downloaded on demand to the current device.

    Copyright (C) 2015 Jörgen Karlsson. All rights reserved.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Supported platforms

Currently tested on Linux/debian with Python 2.7.

# Installation

## Prerequsites
 
Linux with installed fuse. 

## fuse

Example for debian/ubuntu:

	sudo apt-get install fuse

##Install Python and pip

Note that it is likely that both Python and pip already are installed, depending on what linux distribution you have. If not follow below instructions:

###Python 2.7

Please refer to [Python download]( https://www.python.org/download/) for installation instructions. Note that you shall install 2.7. (3.x is not yet supported.)

###pip

Refer to [pip documentation](http://pip.readthedocs.org/en/latest/installing.html) for installation instructions. Note, do not install debian/ubuntu package python-pip, it has a bug preventing installation, see https://bugs.launchpad.net/ubuntu/+source/python-pip/+bug/1306991 

Example:
    
    wget https://raw.github.com/pypa/pip/master/contrib/get-pip.py
	sudo python get-pip.py
	
##Install stmount

### Install virtualenv

Although not a requirement it is recommended to install `virtualenv`, to isolate the python environment for stmount. 

To install virtualenv:

	[sudo] pip install virtualenv

### Create directory and virtualenv

Create a directory for syncthing-fs. You can call this anything, but recommended is "syncthing-fs"

    mkdir ~/syncthing-fs

Change default to the newly created directory and create a virtualenv. Activate the virtual environment.

    cd ~/syncthing-fs
    virtualenv env
    source env/bin/activate

### Install syncthing.fs

Syncthng-fs is installed via pip. Make sure you have activated the virtualenv before installing homeserver.

To install latest development version:

    pip install https://bitbucket.org/jorkar/syncthing-fs/get/master.tar.gz 
    
If you did not install virtualenv, add sudo to the above command.

# Getting Started

## Make sure you can run fuse

On some systems (e.g. ubuntu) you need to add your user to the fuse group:

	sudo addgroup <username> fuse

(Replace <username> with your username or the the username of the user you want to add to fuse)

## Prepare folder
Prepare a folder to mount in syncthing. The best way to show what syncthing-fs can do is to share a new folder to the current device. In that .stignore you add one line:

	*
Example:

	echo "*" > .stignore
	
This will effectively not sync any files at all. However, after mounting, synchting-fs will make syncthing sync on demand.

## stmount

	usage: stmount [-h] [-b] [-d] [-t] [-v] [-a ADDRESS] [-k APIKEY]
	               stfolder mntpnt
	
	stmount version 0.1.0
	
	positional arguments:
	  stfolder              name of Syncthing folder to mount
	  mntpnt                mount point
	
	optional arguments:
	  -h, --help            show this help message and exit
	  -b, --background      Run in background
	  -d, --debug           Enable debug logs
	  -t, --test            Run tests and exit
	  -v, --version         show program's version number and exit
	  -a ADDRESS, --address ADDRESS
	                        Address or name of host to connect to, default what
	                        was given previous invocation or http://localhost:8234
	  -k APIKEY, --key APIKEY
	                        API-key for syncthing, default to what was given
	                        previous invocation


Example. Mount your shared directory "kalle" on mount point /mnt/kalle. Syncthing api on localhost port 8080.

	stmount kalle /mnt/kalle -a http://localhost:8080 -k 05bs1lyDvZVHJNz0BjR69S3SfXgEPJA6

After running the first time, adress and api-key can be removed, since they are stored in ~/.syncthing-fs/config

	stmount kalle /mnt/kalle


## Unmounting

If run in background you can dismount the filesystem via fusermount:

	fusermount -u <mntpnt>

# Known faults
This is a prototype. Error checking is not complete. Use with care.

* Only reading is supported. Writing works, however updates are not always done correct to the cluster.
* Deleting is not supported. Odd things happen currently. 
* The .stignore file will contain more and more rows. This can slow down performance of Syncthing. Solution: adjust manually.
* Run in background not working (disabled due to a minor bug, will soon be fixed)

## Known issues 

* Large and huge folders will take some time when accessing a file first time (eg via ls/dir). This is due to a lot of data beeing fetched from Syncthing, and has to do with how Syncthing works. After first fetch of a directory the data will be cached, however when a change occurs in that directory, it has to be re-fetched.


#Release notes

## 0.1.0
Proof of concept
