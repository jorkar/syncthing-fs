#!env/bin/python
# -*- coding: utf-8 -*-
"""
    stfs

    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import with_statement

from errno import EACCES, ENOENT
from threading import Lock, Event
import os

from fuse import FuseOSError, Operations, LoggingMixIn
import requests
from datetime import datetime
from dateutil import parser

from __builtin__ import file

from stlogging import Logging

class SyncthingAPI(LoggingMixIn):
    
    def __init__(self, address, key):
        self._headers = { "X-API-key": key}
        self.address = address
        self.count_get = 0
        self.count_post = 0
        self.session = requests.Session()
        config = self.get_system_config()
        realkey = config["gui"]["apiKey"]
        if realkey != key:
            raise Exception('API key is invalid, you should use "%s"'%(realkey))

    def get(self, endpoint, params = None):
        r = self.session.get(self.address + endpoint, params = params, headers = self._headers)
        self.count_get += 1
        self.log.debug("Get no %u", self.count_get)
        return r.json()
    
    def post(self, endpoint, params = None, data = None):
        r = self.session.post(self.address + endpoint, params = params, 
                              json = data,
                              headers = self._headers)
        self.count_post += 1
        self.log.debug("Post no %u", self.count_post)
        print r
        return r.json()

    def get_version(self):
        return self.get("/rest/system/version")

    def get_folders(self):
        return self.get("/rest/stats/folder")
    
    def get_system_config(self):
        # todo cache could be needed.
        return self.get("/rest/system/config")
    
    def get_db_browse(self, folder, prefix="", levels=0):
        """ note: this is expensive """
        return self.get("/rest/db/browse", { 'folder' : folder, 'prefix' : prefix, 'levels' : levels } )
    
    def get_db_file(self, folder, path):
        return self.get("/rest/db/file", {'folder' : folder, 'file' : path})
    
    def get_events(self, params):
        return self.get("/rest/events", params)
    
    def get_ignores(self, folder):
        return self.get("/rest/db/ignores", { 'folder' : folder } )
        
    def post_ignores(self, folder, data):
        return self.post("/rest/db/ignores", { 'folder' : folder } , data)
    
        
class SyncthingFolder(Logging):
    
    def __init__(self, api, folderName):
        self.folderName = folderName
        self.api = api
        config = api.get_system_config()
        self.config = [x for x in config['folders'] if x['id'] == folderName][0]
        self._dirs = {}
        self._files = {}
        self.rwlock = Lock()
        self._events = {}
        
        
    def get_config_path(self):
        return self.config['path']
    
    def fix_path(self, path):
        p = os.path.relpath(path, self.get_config_path())
        if p == ".":
            p = ""
        return p
    
    def get_files(self, path):
        with self.rwlock:
            if path not in self._dirs:
                self._dirs[path] = [x for x in self.api.get_db_browse(self.folderName, self.fix_path(path))]
                self.log.debug("Directory for %s now %s", path, str(self._dirs[path]))
            return self._dirs[path]
    
    def get_file(self, path):
        with self.rwlock:
            if path not in self._files:
                relpath = self.fix_path(path)
                data =  self.api.get_db_file(self.folderName, relpath )
                self._files[path] = SyncthingFile(path, relpath, data)
            return self._files[path]
        
    def invalidate_dir(self, dir):
        with self.rwlock:
            try:
                del self._dirs[dir]
            except KeyError:
                pass

    def invalidate_file(self, file):
        with self.rwlock:
            try:
                del self._files[file]
            except KeyError:
                pass

    def invalidate(self):
        """ invalidate all files and folders in cache """
        with self.rwlock:
            self._files = {}
            self._dirs = {}
            
    def fetch_file(self, path):
        # start by ensuring that the directory exists
        if not os.path.exists(path):
            os.makedirs(path)
        # then we check if the file is not already exmtped
        ignores = self.api.get_ignores(self.folderName)
        p = self.fix_path(path)
        ifile = "!/" + p
        if ifile not in ignores["ignore"]:
            del ignores["patterns"]
            ignores["ignore"].insert(0, ifile)
            self.api.post_ignores(self.folderName, ignores)
        # todo prioritize it
        with self.rwlock:
            self._events[p] = Event()
        # wait till it arrives
        print "** waiting for %s"%(p)      
        self._events[p].wait(60)
        with self.rwlock:
            print "** dellet lock for %s"%(p)      
            del self._events[p]

    def send_event_arrived(self, p):
        print "** checking %s"%(p)      
        with self.rwlock:
            if p in self._events:
                print "** releasefor %s"%(p)      
                self._events[p].set()
        
        
    
class SyncthingFile():
    # Flags, from protocol.go
    #     const (
    #     FlagDeleted              uint32 = 1 << 12
    #     FlagInvalid                     = 1 << 13
    #     FlagDirectory                   = 1 << 14
    #     FlagNoPermBits                  = 1 << 15
    #     FlagSymlink                     = 1 << 16
    #     FlagSymlinkMissingTarget        = 1 << 17
    # 
    #     FlagsAll = (1 << 18) - 1
    # 
    #     SymlinkTypeMask = FlagDirectory | FlagSymlinkMissingTarget
    # )
    FLAG_DELETE                     = 1 << 12 
    FLAG_INVALID                    = 1 << 13
    FLAG_DIRECTORY                  = 1 << 14
    FLAG_NOPERMBITS                 = 1 << 15
    FLAG_SYMLINK                    = 1 << 16
    FLAG_SYMLINK_MISSING_TARGET     = 1 << 17
    MASK_PERMISSION                 = 0777
    # st_mode, from linux man stat
    #     S_IFSOCK   0140000   socket
    #     S_IFLNK    0120000   symbolic link
    #     S_IFREG    0100000   regular file
    #     S_IFBLK    0060000   block device
    #     S_IFDIR    0040000   directory
    #     S_IFCHR    0020000   character device
    #     S_IFIFO    0010000   FIFO
    S_IFLNK = 0120000   
    S_IFREG = 0100000
    S_IFDIR = 0040000

    
    def __init__(self, path, relpath, data):
        self.path = path
        self.relpath = relpath
        self.fileInfo = data
        self._global_mtime = None
    
    def get_global_mtime(self):
        if self._global_mtime is None:
            self._global_mtime = self._to_ts(self.fileInfo['global']['modified'])
        return self._global_mtime
    
    def get_global_size(self):
        return self.fileInfo['global']['size']
        
    def _to_ts(self, date):
        """ take string date and convert to timestamp """
        dt = parser.parse(date)
        utc_naive  = dt.replace(tzinfo=None) - dt.utcoffset()
        timestamp = (utc_naive - datetime(1970, 1, 1)).total_seconds()
        return timestamp
    
    def local_is_current(self):
        global_mtime = self.get_global_mtime()
        # TODO the if statement should compare versions, see documentation
        return os.path.exists(self.path) and (os.path.getmtime(self.path) >= global_mtime)

    def is_flag(self, flag):
        return int(self.fileInfo["global"]["flags"],8) & flag == flag
    
    def is_directory(self):
        return self.is_flag(SyncthingFile.FLAG_DIRECTORY)
    
    def get_global_mode(self):
        #TODO fix, directory flag and permissions
        p = int(self.fileInfo["global"]["flags"],8) & SyncthingFile.MASK_PERMISSION
        if (self.is_directory()):
            p |= SyncthingFile.S_IFDIR
            # todo, symlink
        else:
            p |=  SyncthingFile.S_IFREG
        return p
    
    def stat(self):
        if self.local_is_current():
            st = os.lstat(self.path)     
            rv = dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))
            return rv
        elif self.fileInfo['global']['name'] == "":
            # file do not exist in global nor local
            raise FuseOSError(ENOENT)
        else:
            # we do not have file in cache
            return {'st_ctime': 0, 
                    'st_mtime': self.get_global_mtime(), 
                    'st_nlink': 0, 
                    'st_mode': self.get_global_mode(), 
                    'st_size': self.get_global_size(), 
                    'st_gid': os.getgid(), 
                    'st_uid': os.getuid(), 
                    'st_atime': 0}

            

from sys import getfilesystemencoding
    
class SyncthingFS(LoggingMixIn, Operations):
    def __init__(self, folder):
        self.syncthingFolder = folder
        self.root = folder.get_config_path()
        self.rwlock = Lock()

    def __call__(self, op, path, *args):
        return super(SyncthingFS, self).__call__(op, self.root + path, *args)

    def access(self, path, mode):
        if not os.access(path, mode):
            raise FuseOSError(EACCES)

    chmod = os.chmod
    chown = os.chown

    def create(self, path, mode):
        return os.open(path, os.O_WRONLY | os.O_CREAT, mode)

    def flush(self, path, fh):
        return os.fsync(fh)

    def fsync(self, path, datasync, fh):
        return os.fsync(fh)

    def getattr(self, path, fh=None):
        fileInfo = self.syncthingFolder.get_file(path)
        return fileInfo.stat()


    getxattr = None

    def link(self, target, source):
        return os.link(source, target)

    listxattr = None
    mkdir = os.mkdir
    mknod = os.mknod
    
    def _ensure_exists(self, path):
        fileInfo = self.syncthingFolder.get_file(path)
        if not fileInfo.local_is_current():
            self.syncthingFolder.fetch_file(path)
            # TODO, wait till it exists
            
    def open(self, path, flags):
        '''
        When raw_fi is False (default case), open should return a numerical
        file handle.

        When raw_fi is True the signature of open becomes:
            open(self, path, fi)

        and the file handle should be set directly.
        '''
        self.log.debug(" Open %s", path)
        self._ensure_exists(path)
        return os.open(path, flags)

    def read(self, path, size, offset, fh):
        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.read(fh, size)

    def readdir(self, path, fh):
        global_files = self.syncthingFolder.get_files(path)
        try:
            local_files = os.listdir(path)
        except: 
            local_files = []
        rv = ['.', '..'] + list(set(global_files + local_files))
        return rv

    readlink = os.readlink

    def release(self, path, fh):
        return os.close(fh)

    def rename(self, old, new):
        return os.rename(old, self.root + new)

    rmdir = os.rmdir

    def statfs(self, path):
        path = path.encode(getfilesystemencoding())
        stv = os.statvfs(path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    def symlink(self, target, source):
        return os.symlink(source, target)

    def truncate(self, path, length, fh=None):
        with open(path, 'r+') as f:
            f.truncate(length)

    unlink = os.unlink
    utimens = os.utime

    def write(self, path, data, offset, fh):
        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.write(fh, data)



from threading import Thread

class EventsThread(Thread, LoggingMixIn):
    
    def __init__(self, folder):
        self.folder = folder
        self.api = folder.api
        super(EventsThread, self).__init__()
        
    def run(self):
        params = {"since" : 0 , "limit" : 1}
        first = True
        while 1:
            try:
                events = self.api.get_events(params)
                for event in events:
                    params["since"] = event.get("id") or params["since"]
                    data = event.get("data")
                    if data and data.get("folder") == self.folder.folderName:
                        self.handle(event.get("type"), data)
                if first:
                    del params["limit"]
                    first = False
            except:
                raise
            

    def handle(self, type, data):
        self.log.debug("event %s %s", type, str(data))
        if type == "ItemFinished":
            self.log.info("Finished %s %s", data.get("type"), data.get("item"))
            if data.get("type") == "dir": 
                self.folder.invalidate_dir(data.get("item" ))
            elif data.get("type") == "file": 
                self.folder.invalidate_file(data.get("item" ))
                self.folder.send_event_arrived(data.get("item"))
        elif type == "RemoteIndexUpdated":
            self.folder.invalidate()

