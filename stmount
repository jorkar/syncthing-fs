#!env/bin/python
# -*- coding: utf-8 -*-
"""
    stmount
    ~~~~~~~

    Main prog for syncthing-fs
    
    :copyright: (c) 2014 Jörgen Karlsson

    :license: 
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
    You should have received a copy of the GNU Affero General Public License
    along with this program (LICENCE.txt)  If not, see <http://www.gnu.org/licenses/>.
"""

from sys import exit
import os
from fuse import FUSE
from argparse import ArgumentParser, ArgumentTypeError
import logging

from stfs import SyncthingAPI, SyncthingFolder, SyncthingFS, EventsThread
from stfs.version import __version__

from util import Config

def version():
    return "stmount version %s" % (__version__)

def mountpoint(dir_):
    if not os.path.isdir(dir_):
        raise ArgumentTypeError( 'dir has to be a directory' )
    return dir_
    
def main():
    config = Config.instance(os.path.expanduser("~/.syncthing-fs/config"))
    parser = ArgumentParser(description = version())
    parser.add_argument("-b", "--background", action = "store_true", help = "Run in background")
    parser.add_argument("-d", "--debug", action = "store_true", help = "Enable debug logs")
    parser.add_argument("-t", "--test", action = "store_true", help = "Run tests and exit")
    #parser.add_argument("-c", "--config", action="store", dest="config", default = ".stfs", help="Use configuration file CONFIG")
    parser.add_argument("-v", "--version", action="version", version=version())
    parser.add_argument("-a", "--address", action="store", dest="address", 
                        help="Address or name of host to connect to, default what was given previous invocation or http://localhost:8234")
    parser.add_argument("-k", "--key", action="store", dest="apikey", help="API-key for syncthing, default to what was given previous invocation")
    parser.add_argument("stfolder", help="name of Syncthing folder to mount")
    parser.add_argument("mntpnt", help="mount point", type=mountpoint)
    args=parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    if args.address:
        config.set("syncthing", "address", args.address)
    if args.apikey:
        config.set("syncthing", "apikey", args.apikey)
    address = config.get("syncthing", "address", "http://localhost:8234" )
    apikey =  config.get("syncthing", "apikey") 
    config.write()
    api = SyncthingAPI(address, apikey)
    print "Connected to %s (%s)" % (api.address, api.get_version()['version'])

    folders = api.get_folders()
    if not args.stfolder in folders:
        print "Folder not defined, defined folders are:"
        for f in folders:
            print " " + f
        exit(1)

    if args.background:
        print "Background currently not supported in this version"
        exit(1)
    folder = SyncthingFolder(api, args.stfolder)
    fs = SyncthingFS(folder)
    if args.test:
        print "Tests not available"
    else:
        print "Starting Events Thread"
        # TODO events thread shall be started by the SyncthingFolder so that we can support background.
        e = EventsThread(folder)
        e.daemon = True
        e.start()
        print "Mounting Syncthing folder %s on %s " % (args.stfolder, args.mntpnt)
        fuse = FUSE(fs, args.mntpnt, foreground=not args.background)

if __name__ == "__main__":
    main()
